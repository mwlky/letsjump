// Fill out your copyright notice in the Description page of Project Settings.


#include "Checkpoint.h"

#include "Kismet/GameplayStatics.h"

// Sets default values
ACheckpoint::ACheckpoint()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	TriggerCapsule = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Trigger Capsule"));
	RootComponent = TriggerCapsule;

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void ACheckpoint::BeginPlay()
{
	Super::BeginPlay();
	
	GameMode = Cast<ALetsJumpGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));
}

// Called every frame
void ACheckpoint::Tick(float DeltaTime)
{
	// Tick is turned off
	
	Super::Tick(DeltaTime);

}

void ACheckpoint::PlayerGotNewCheckPoint()
{
	if(!GameMode) return;
	
	GameMode->SetNewPlayerCheckpoint(GetActorLocation());
	Destroy();
}
