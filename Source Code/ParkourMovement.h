// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "ParkourMovement.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class LETSJUMP_API UParkourMovement : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UParkourMovement();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable)
	void Initialize(ACharacter* Character);

	UFUNCTION(BlueprintCallable)
	void LandEvent();

	UFUNCTION(BlueprintCallable)
	void JumpEvent();

	UFUNCTION(BlueprintCallable)
	void WallRunUpdateEvent();

	
private:
	
	ACharacter* PlayerCharacter = nullptr;
	UCharacterMovementComponent* CharacterMovement = nullptr;

	float DefaultGravity;
};
