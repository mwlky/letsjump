// Fill out your copyright notice in the Description page of Project Settings.


#include "ParkourMovement.h"

#include "GameFramework/Character.h"

// Sets default values for this component's properties
UParkourMovement::UParkourMovement()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UParkourMovement::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UParkourMovement::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UParkourMovement::Initialize(ACharacter* Character)
{
	PlayerCharacter = Character;
	CharacterMovement = Character->FindComponentByClass<UCharacterMovementComponent>();

	DefaultGravity = CharacterMovement->GravityScale;

	UE_LOG(LogTemp, Warning, TEXT("%f"), DefaultGravity);

	FTimerHandle TimerHandle;
	
	GetWorld()->GetTimerManager().SetTimer(TimerHandle, this, &UParkourMovement::WallRunUpdateEvent, 0.2f, true, 0.0f);
}

void UParkourMovement::JumpEvent()
{
	
}

void UParkourMovement::LandEvent()
{
	
}

void UParkourMovement::WallRunUpdateEvent()
{
	UE_LOG(LogTemp, Warning, TEXT("Hello world!"));
}





