// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerKillObject.h"

#include "Kismet/GameplayStatics.h"

// Sets default values
APlayerKillObject::APlayerKillObject()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Collider = CreateDefaultSubobject<UBoxComponent>(TEXT("Box Collision"));
	RootComponent = Collider;

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void APlayerKillObject::BeginPlay()
{
	Super::BeginPlay();

	GameMode = Cast<ALetsJumpGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));
}

// Called every frame
void APlayerKillObject::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APlayerKillObject::KillPlayer()
{
	GameMode->KillPlayer();
}
