// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "LetsJumpGameModeBase.h"
#include "Components/BoxComponent.h"
#include "GameFramework/Actor.h"
#include "PlayerKillObject.generated.h"

UCLASS()
class LETSJUMP_API APlayerKillObject : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APlayerKillObject();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void KillPlayer();

private:

	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* Mesh = nullptr;

	UPROPERTY(EditAnywhere)
	UBoxComponent* Collider = nullptr;

	UPROPERTY()
	ALetsJumpGameModeBase* GameMode = nullptr;

	

};
