// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerCharacter.h"

#include "ParkourPlayerController.h"
#include "GameFramework/CharacterMovementComponent.h"

// Sets default values
APlayerCharacter::APlayerCharacter()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
}

// Called when the game starts or when spawned
void APlayerCharacter::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void APlayerCharacter::Tick(float DeltaTime)
{
	// Tick is turned off
	
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void APlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis(TEXT("MoveForward"), this, &APlayerCharacter::PlayerMoveForward);
	PlayerInputComponent->BindAxis(TEXT("MoveBackward"), this, &APlayerCharacter::PlayerMoveBackward);
	PlayerInputComponent->BindAxis(TEXT("MoveLeft"), this, &APlayerCharacter::PlayerTurnLeft);
	PlayerInputComponent->BindAxis(TEXT("MoveRight"), this, &APlayerCharacter::PlayerTurnRight);
	PlayerInputComponent->BindAxis(TEXT("MouseX"), this, &APlayerCharacter::PlayerLookRight);
	PlayerInputComponent->BindAxis(TEXT("MouseY"), this, &APlayerCharacter::PlayerLookUp);
	PlayerInputComponent->BindAction(TEXT("EnableMenu"), IE_Pressed, this, &APlayerCharacter::EnableMenu);
}

void APlayerCharacter::EnableMenu()
{
	AParkourPlayerController* ParkourPlayerController = Cast<AParkourPlayerController>(GetWorld()->GetFirstPlayerController());

	if(ParkourPlayerController)
	{
		ParkourPlayerController->MenuHandle();
	}
}


void APlayerCharacter::PlayerMoveForward(float Input)
{
	if (!bIsClimbing)
		AddMovementInput(GetActorForwardVector() * Input);;
}

void APlayerCharacter::PlayerMoveBackward(float Input)
{
	if (!bIsClimbing)
		AddMovementInput(GetActorForwardVector() * Input);
}

void APlayerCharacter::PlayerLookUp(float Input)
{
	AddControllerPitchInput(-Input * RotationRate * GetWorld()->GetDeltaSeconds());
}

void APlayerCharacter::PlayerLookRight(float Input)
{
	AddControllerYawInput(Input * RotationRate * GetWorld()->GetDeltaSeconds());
}

void APlayerCharacter::PlayerTurnLeft(float Input)
{
	if (!bIsClimbing)
		AddMovementInput(GetActorRightVector() * Input);
}

void APlayerCharacter::PlayerTurnRight(float Input)
{
	if (!bIsClimbing)
		AddMovementInput(GetActorRightVector() * Input);
}
