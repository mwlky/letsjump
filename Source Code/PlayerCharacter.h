// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/Character.h"
#include "PlayerCharacter.generated.h"

UCLASS()
class LETSJUMP_API APlayerCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	APlayerCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float PlayerWalkSpeed = 20.f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float PlayerSprintSpeed = 40.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bIsClimbing;
	
private:
	
	void PlayerMoveForward(float Input);
	void PlayerMoveBackward(float Input);
	void PlayerTurnLeft(float Input);
	void PlayerTurnRight(float Input);
	void PlayerLookUp(float Input);
	void PlayerLookRight(float Input);
	void EnableMenu();

	UPROPERTY(EditAnywhere)
	float RotationRate = 10.f;

};
