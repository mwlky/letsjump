// Fill out your copyright notice in the Description page of Project Settings.


#include "Point.h"

#include "LetsJumpGameModeBase.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
APoint::APoint()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CapsuleTrigger = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Trigger Capsule"));
	RootComponent = CapsuleTrigger;

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void APoint::BeginPlay()
{
	Super::BeginPlay();

	GameMode = Cast<ALetsJumpGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));
}

// Called every frame
void APoint::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}
void APoint::CollectPoint()
{
	if (GameMode)
	{
		GameMode->PointCollected(PointValue);
		UGameplayStatics::PlaySound2D(this, CollectSFX);

		Destroy();
		
		// FTimerHandle TimerHandle;
		// GetWorld()->GetTimerManager().SetTimer(TimerHandle, this, &APoint::DestroyActor, CollectSFX->GetDuration());
	}
}
