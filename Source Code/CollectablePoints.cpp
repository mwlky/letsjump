// Fill out your copyright notice in the Description page of Project Settings.


#include "CollectablePoints.h"

// Sets default values
ACollectablePoints::ACollectablePoints()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CapsuleTrigger = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Capsule Trigger"));
	RootComponent = CapsuleTrigger;

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(CapsuleTrigger);

}

// Called when the game starts or when spawned
void ACollectablePoints::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACollectablePoints::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ACollectablePoints::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

