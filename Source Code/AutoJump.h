// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PlayerCharacter.h"
#include "Components/ActorComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "AutoJump.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class LETSJUMP_API UAutoJump : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UAutoJump();

	UFUNCTION(BlueprintCallable)
	void GetJumpButton(bool IsPressed);

	UFUNCTION(BlueprintCallable)
	void GetCharacterMovementComponent(UCharacterMovementComponent* CharacterMovementComponent);
	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

private:

	UPROPERTY()
	UCharacterMovementComponent* MovementComponent = nullptr;

	UPROPERTY()
	APlayerController* PlayerController = nullptr;

	UPROPERTY()
	ACharacter* PlayerCharacter = nullptr;

	UPROPERTY()
	bool bPlayerWantsJump;
};
