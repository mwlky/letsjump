// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "PlayerCharacter.h"
#include "GameFramework/GameModeBase.h"
#include "LetsJumpGameModeBase.generated.h"


UCLASS()
class LETSJUMP_API ALetsJumpGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

private:

	UPROPERTY()
	APlayerCharacter* PlayerCharacter = nullptr;
	UPROPERTY()
	UUserWidget* DuringGameWidget = nullptr;
	UPROPERTY()
	UUserWidget* NotEnoughPointsWidget = nullptr;
	UPROPERTY()
	APlayerController* PlayerController = nullptr;

	UPROPERTY()
	FVector PlayerCheckPointLocation;
	
	UPROPERTY(EditAnywhere)
	TSubclassOf<UUserWidget> CompleteLevelUIClass;
	UPROPERTY(EditAnywhere)
	TSubclassOf<UUserWidget> DuringGameWidgetClass;
	UPROPERTY(EditAnywhere)
	TSubclassOf<UUserWidget> NotEnoughPointsClass;
	UPROPERTY(EditAnywhere)
	TSubclassOf<UUserWidget> CheckpointReachedClass;

	
	UFUNCTION()
	void CountAllPoints();
	UFUNCTION()
	void DisplayInGameWidget();
	UFUNCTION()
	void SetPlayerBeginCheckpointLocation();
	UFUNCTION()
	void GetPlayer();
	UFUNCTION()
	void ConstructNotEnoughPointsWidget();
	UFUNCTION()
	void ShowNotEnoughPointsWidget(bool bShow) const;
	UFUNCTION()
	void ShowCheckpointReachedWidget(float Delay) const;
	
protected:
	virtual void BeginPlay() override;

	virtual void Tick(float DeltaSeconds) override;

public:
	
	ALetsJumpGameModeBase();

	UPROPERTY(BlueprintReadOnly)
	int32 CollectedPoints;
	UPROPERTY(BlueprintReadOnly)
	int32 NumberOfPointsOnTheMap;
	UPROPERTY(BlueprintReadOnly)
	int32 PlayerDeaths = 0;
	UPROPERTY(BlueprintReadOnly)
	float TimeElapsed;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float DelayToShow;
	
	UFUNCTION()
	bool GameEndCheck();
	UFUNCTION()
	void SetNewPlayerCheckpoint(FVector Location);
	UFUNCTION()
	void PlayerCompletedLevel();
	UFUNCTION()
	void KillPlayer();
	UFUNCTION()
	void GameEndedUI() const;

	UFUNCTION(BlueprintCallable)
	void PointCollected(int32 Value);
	UFUNCTION(BlueprintCallable)
	void InitializeNewLevel();
};