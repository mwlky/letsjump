// Fill out your copyright notice in the Description page of Project Settings.


#include "AutoJump.h"

#include <gameux.h>

#include "GameFramework/Character.h"
#include "Kismet/GameplayStatics.h"

UAutoJump::UAutoJump()
{
	PrimaryComponentTick.bCanEverTick = true;
}


// Called when the game starts
void UAutoJump::BeginPlay()
{
	Super::BeginPlay();

	PlayerController = UGameplayStatics::GetPlayerController(this, 0);
	PlayerCharacter = UGameplayStatics::GetPlayerCharacter(this, 0);
}


void UAutoJump::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (!MovementComponent || !PlayerController) return;

	if (!MovementComponent->IsFalling() && PlayerCharacter && bPlayerWantsJump)
	{
		PlayerCharacter->Jump();
	}
}

void UAutoJump::GetCharacterMovementComponent(UCharacterMovementComponent* CharacterMovementComponent)
{
	MovementComponent = CharacterMovementComponent;
}

void UAutoJump::GetJumpButton(bool IsPressed)
{
	bPlayerWantsJump = IsPressed;
}
