// Fill out your copyright notice in the Description page of Project Settings.


#include "CompleteLevel.h"

#include "Kismet/GameplayStatics.h"

// Sets default values
ACompleteLevel::ACompleteLevel()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CapsuleComponent = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Collision Trigger"));
	RootComponent = CapsuleComponent;

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void ACompleteLevel::BeginPlay()
{
	Super::BeginPlay();

	GameMode = Cast<ALetsJumpGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));
}

// Called every frame
void ACompleteLevel::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ACompleteLevel::CompleteLevel()
{
	if (GameMode)
	{
		if(GameMode->GameEndCheck())
			Destroy();
	}
}
