// Copyright Epic Games, Inc. All Rights Reserved.

#include "LetsJump.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, LetsJump, "LetsJump" );
