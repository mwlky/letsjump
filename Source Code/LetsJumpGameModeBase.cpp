// Copyright Epic Games, Inc. All Rights Reserved.


#include "LetsJumpGameModeBase.h"

#include "Kismet/GameplayStatics.h"
#include "Blueprint/UserWidget.h"
//#include "ToolBuilderUtil.h"
#include "Point.h"

ALetsJumpGameModeBase::ALetsJumpGameModeBase()
{
	PrimaryActorTick.bCanEverTick = true;
}

void ALetsJumpGameModeBase::BeginPlay()
{
	Super::BeginPlay();
}

void ALetsJumpGameModeBase::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
}

void ALetsJumpGameModeBase::PointCollected(int32 Value)
{
	CollectedPoints += Value;
}

void ALetsJumpGameModeBase::SetNewPlayerCheckpoint(FVector Location)
{
	PlayerCheckPointLocation = Location;

	ShowCheckpointReachedWidget(DelayToShow);
}

void ALetsJumpGameModeBase::KillPlayer()
{
	if (!PlayerCharacter) return;
	PlayerCharacter->SetActorLocation(PlayerCheckPointLocation);

	PlayerDeaths++;
}

void ALetsJumpGameModeBase::PlayerCompletedLevel()
{
	GameEndedUI();
	DuringGameWidget->RemoveFromParent();
	ShowNotEnoughPointsWidget(false);

	TimeElapsed = PlayerCharacter->GetWorld()->GetTimeSeconds();
}

void ALetsJumpGameModeBase::CountAllPoints()
{
	TArray<AActor*> Points;

	UGameplayStatics::GetAllActorsOfClass(this, APoint::StaticClass(), Points);

	NumberOfPointsOnTheMap = Points.Num();
}

void ALetsJumpGameModeBase::DisplayInGameWidget()
{
	if (!PlayerController) return;

	DuringGameWidget = CreateWidget(PlayerController, DuringGameWidgetClass);

	if (DuringGameWidget)
		DuringGameWidget->AddToViewport();
}

void ALetsJumpGameModeBase::SetPlayerBeginCheckpointLocation()
{
	if (PlayerCharacter)
		PlayerCheckPointLocation = PlayerCharacter->GetActorLocation();
}

void ALetsJumpGameModeBase::InitializeNewLevel()
{
	PlayerDeaths = 0;
	PlayerController = UGameplayStatics::GetPlayerController(this, 0);

	GetPlayer();
	SetPlayerBeginCheckpointLocation();
	CountAllPoints();
	DisplayInGameWidget();
	ConstructNotEnoughPointsWidget();
}

void ALetsJumpGameModeBase::GameEndedUI() const
{
	if (!PlayerController) return;

	UUserWidget* Widget = CreateWidget(PlayerController, CompleteLevelUIClass);
	if (!Widget) return;

	Widget->AddToViewport();

	PlayerController->bShowMouseCursor = true;

	if (PlayerCharacter)
	{
		PlayerCharacter->DisableInput(PlayerController);
	}
}

bool ALetsJumpGameModeBase::GameEndCheck()
{
	if (CollectedPoints >= NumberOfPointsOnTheMap)
	{
		PlayerCompletedLevel();
		return true;
	}

	ShowNotEnoughPointsWidget(true);
	return false;
}

void ALetsJumpGameModeBase::GetPlayer()
{
	APawn* PlayerPawn = UGameplayStatics::GetPlayerPawn(this, 0);
	if (!PlayerPawn) return;

	APlayerCharacter* ThisPlayerCharacter = Cast<APlayerCharacter>(PlayerPawn);
	if (!ThisPlayerCharacter) return;

	PlayerCharacter = ThisPlayerCharacter;
}

void ALetsJumpGameModeBase::ConstructNotEnoughPointsWidget()
{
	if (!PlayerController) return;

	NotEnoughPointsWidget = CreateWidget(PlayerController, NotEnoughPointsClass);
}

void ALetsJumpGameModeBase::ShowNotEnoughPointsWidget(bool bShow) const
{
	if (bShow && NotEnoughPointsWidget && !NotEnoughPointsWidget->IsVisible())
		NotEnoughPointsWidget->AddToViewport();

	if (!bShow && NotEnoughPointsWidget && NotEnoughPointsWidget->IsVisible())
		NotEnoughPointsWidget->RemoveFromViewport();
}

void ALetsJumpGameModeBase::ShowCheckpointReachedWidget(float Delay) const
{
	if (!PlayerController) return;

	UUserWidget* CheckpointWidget = CreateWidget(PlayerController, CheckpointReachedClass);
	if (!CheckpointWidget) return;

	CheckpointWidget->AddToViewport();
}
