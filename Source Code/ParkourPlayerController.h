// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "ParkourPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class LETSJUMP_API AParkourPlayerController : public APlayerController
{
	GENERATED_BODY()

public:

	AParkourPlayerController();

	UFUNCTION(BlueprintCallable)
	float GetTimeElapsed();

	UFUNCTION(BlueprintCallable)
	void InputHandle(bool bEnable);

	UFUNCTION()
	void MenuHandle();

private:

	virtual void Tick(float DeltaSeconds) override;
	virtual void BeginPlay() override;

	void ShowMenu();
	void CloseMenu();


	bool bIsMenuEnable = false;

	UPROPERTY(EditAnywhere)
	TSubclassOf<UUserWidget> MenuWidgetClass;

	UUserWidget* MenuWidget;
};
