// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "LetsJumpGameModeBase.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/Actor.h"
#include "Sound/SoundCue.h"
#include "Point.generated.h"

UCLASS()
class LETSJUMP_API APoint : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APoint();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UStaticMeshComponent* Mesh;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UCapsuleComponent* CapsuleTrigger;

	UFUNCTION(BlueprintCallable)
	void CollectPoint();
	
private:

	UPROPERTY(EditAnywhere)
	int32 PointValue = 1;

	UPROPERTY()
	ALetsJumpGameModeBase* GameMode = nullptr;

	UPROPERTY(EditAnywhere)
	USoundCue* CollectSFX;
};
