// Fill out your copyright notice in the Description page of Project Settings.


#include "ParkourPlayerController.h"

#include "Blueprint/UserWidget.h"
#include "Kismet/GameplayStatics.h"

AParkourPlayerController::AParkourPlayerController()
{
	PrimaryActorTick.bCanEverTick = true;
}

void AParkourPlayerController::BeginPlay()
{
	Super::BeginPlay();

	MenuWidget = CreateWidget(this, MenuWidgetClass);
}

void AParkourPlayerController::Tick(float DeltaSeconds)
{
	
	Super::Tick(DeltaSeconds);
}

float AParkourPlayerController::GetTimeElapsed()
{
	return GetWorld()->GetDeltaSeconds();
}

void AParkourPlayerController::MenuHandle()
{
	if (!bIsMenuEnable)
	{
		ShowMenu();
	}

	else if (bIsMenuEnable)
	{
		CloseMenu();
	}
}

void AParkourPlayerController::ShowMenu()
{
	bIsMenuEnable = true;

	if (MenuWidget)
		MenuWidget->AddToViewport();

	//Turning off player input and showing mouse coursor
	InputHandle(false);
}

void AParkourPlayerController::CloseMenu()
{
	bIsMenuEnable = false;

	if (MenuWidget)
		MenuWidget->RemoveFromParent();

	//Turning on player input and not showing mouse coursor
	InputHandle(true);
}

void AParkourPlayerController::InputHandle(bool bEnable)
{
	if (bEnable)
	{
		UGameplayStatics::GetPlayerPawn(this, 0)->EnableInput(this);
		bShowMouseCursor = false;
	}

	if (!bEnable)
	{
		UGameplayStatics::GetPlayerPawn(this, 0)->DisableInput(this);
		bShowMouseCursor = true;
	}
}
